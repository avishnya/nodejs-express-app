module.exports = {
    PORT: process.env.NODE_PORT || 80,
    HOST: process.env.NODE_HOST || 'localhost'
};
