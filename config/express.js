var config = require('./config');
var express = require('express');
var logger = require('morgan');
var compress = require('compression');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');

module.exports = function(){
    
    var app = express();

    if(process.env.NODE_ENV == 'development'){
        app.use(logger('dev'));
    }else{
        app.use(compress());
    }

    app.use(bodyParser.urlencoded({ extended:true}));
    app.use(bodyParser.json());
    app.use(methodOverride());

    app.get('/', (req, res) => {
        res.send(`Hello ${req.query.name || 'World'}!`);
    });

    return app;
};