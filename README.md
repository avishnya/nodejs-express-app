# **nodejs-express-app**
A simple node js app using express.js, http-server, and etc.

## **Getting Started**
Before getting started with the project please follow the below steps.

### **Prerquisites**
Before going to install and run the project, there are some things need to know. If you already knew, please go to next step.

#### **Basics**
**Note** : This is for very beginner. If you already knew node, please ignore this.

* [npm](https://www.npmjs.com/) - Node package manager

* [node](https://nodejs.org/en/) - If you don't have node installed, you can donwload [here](https://nodejs.org/en/)

* [express.js](https://expressjs.com/) - Express.js

#### **Packages that really need to know before starting the project**

* [express](https://github.com/expressjs/express)

* [compression](https://github.com/expressjs/compression)

* [body-parser](https://github.com/expressjs/body-parser)

* [morgan](https://github.com/expressjs/morgan)

### **Installation**
**Step 1** : Clone the directory withe the command
```
git clone https://bitbucket.org/avishnya/nodejs-express-app.git
```
cd nodejs-express-app
```

**Step 2** : install node modules needed
```
npm install
```
**Note** : Wait for few minutes to install the node modules, may few hours if your internet connection is slow :stuck_out_tongue_winking_eye:

## **Running app**
Just run
```
node app.js
```
It will show the message that

```
App running at http://localhost:3000/
```
### **Test the app**
Open browser and enter url : http://localhost:3000/ and then you should see :
```
Hellow World!
```